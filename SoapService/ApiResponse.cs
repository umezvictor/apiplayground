﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapService
{
    public class ApiResponse<T>
    {
        public int StatusCode { get; set; }
        public T Payload { get; set; }
        public string message { get; set; }
    }
}