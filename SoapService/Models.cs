﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapService
{
    public class Models
    {
        public string ScheduleId { get; set; }
        public string ClientId { get; set; }
        public string PageNumber { get; set; }
        public string Status { get; set; }
    }


    public class PaymentStatusResponse
    {
        
        public string ScheduleId { get; set; }
        public string ClientId { get; set; }
        public string PageNumber { get; set; }
        public string Status { get; set; }
        public List<PaymentRecord> PaymentRecords { get; set; }
    }


    public class PaymentRecord
    {
       
            public string Beneficiary { get; set; }
            public string Amount { get; set; }
            public string AccountNumber { get; set; }
            public string BankCode { get; set; }
            public string Narration { get; set; }
            public string SerialNo { get; set; }
            public string Status { get; set; }
            public string Reason { get; set; }
    }
   
}