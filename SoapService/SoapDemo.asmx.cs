﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SoapService
{
    /// <summary>
    /// Summary description for SoapDemo
    /// </summary>
      [WebService(Namespace = "http://www.grds.com/")]
      [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
   
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SoapDemo : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public ApiResponse<string> PaymentStatus(PaymentStatusResponse PaymentStatusResponse)
        {
            //foreach(var record in PaymentStatusResponse.PaymentRecords)
            //{
               
            //}

            ApiResponse<string> response = new ApiResponse<string>();

            if(PaymentStatusResponse.Status == "ok" && PaymentStatusResponse.ClientId == "1234")
            {
                response.message = "payment was successful";
                response.StatusCode = 200;
                response.Payload = "victor";
                return response;
            }

            response.message = "error";
            response.StatusCode = 400;

            return response;
        }

        [WebMethod]
        public ApiResponse<string> Login(string email, string password)
        {          
            ApiResponse<string> response = new ApiResponse<string>();

            if (email == "vic@gmaili.com" && password == "1234")
            {
                response.message = "successful";
                response.StatusCode = 200;
                response.Payload = "victor";
                return response;
            }

            response.message = "error";
            response.StatusCode = 400;

            return response;
        }


    }


}
