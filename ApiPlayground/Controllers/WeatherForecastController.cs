﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlayground.Controllers
{
    //[ServiceFilter(typeof(AppActionFilter))] //you can register it at controller level
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IConfiguration configuration;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IConfiguration configuration)
        {
            _logger = logger;
            this.configuration = configuration;
        }

        //[HttpGet]
        //public IEnumerable<WeatherForecast> Get()
        //{
        //    var rng = new Random();
        //    return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    {
        //        Date = DateTime.Now.AddDays(index),
        //        TemperatureC = rng.Next(-20, 55),
        //        Summary = Summaries[rng.Next(Summaries.Length)]
        //    })
        //    .ToArray();
        //}

        [HttpGet]
        [ServiceFilter(typeof(AppActionFilter))] //you can register it at action level
        public IActionResult Get()
        {

            //string config = configuration.GetValue<string>("IpRateLimiting:EnableEndpointRateLimiting");

            int age = 25;
            string name = "Victor";
            //if(age != 24)
            //    throw new AppException("Email or password is incorrect");

            //// a key not found exception that will return a 404 response
            //if(name != "Victor")
            //    throw new KeyNotFoundException("No record found");
            return Ok(new { age = age, name = name });
        }

        [HttpPost]
        [ServiceFilter(typeof(PermissionsFilter))] //you can register it at action level
        public IActionResult Post([FromBody] Model model)
        {

            //string config = configuration.GetValue<string>("IpRateLimiting:EnableEndpointRateLimiting");

            //int age = 25;
            //string name = "Victor";
            //if(age != 24)
            //    throw new AppException("Email or password is incorrect");

            //// a key not found exception that will return a 404 response
            //if(name != "Victor")
            //    throw new KeyNotFoundException("No record found");
            return Ok(new { age = model.Age, name = model.Name });
        }
    }
}
