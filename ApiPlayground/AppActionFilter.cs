﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlayground
{
    public class AppActionFilter : IActionFilter
    {
        //Like the other types of filters, the action filter can be added to different scope levels: Global, Action, Controller.
        public void OnActionExecuted(ActionExecutedContext context)
        {
            throw new NotImplementedException();
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var param = context.ActionArguments.SingleOrDefault();
            if (param.Value == null)
            {
                context.Result = new BadRequestObjectResult("Object is null");
                return;
            }

            if (!context.ModelState.IsValid)
            {
                context.Result = new UnprocessableEntityObjectResult(context.ModelState);
            }

           
        }

        //alternatively
    }

    //use this or the one above, not both
    //public class AsyncActionFilterExample : IAsyncActionFilter
    //{
    //    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    //    {
    //        // execute any code before the action executes
    //        var result = await next();
    //        // execute any code after the action executes
    //    }
    //}

    public class PermissionsFilter : IActionFilter
    {
        //Like the other types of filters, the action filter can be added to different scope levels: Global, Action, Controller.
       

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var param = context.ActionArguments.SingleOrDefault();

            var token = "";
            if (context.HttpContext.Request.Headers.TryGetValue("Token", out var tokenValue))
            {
                token = tokenValue;
            }

            //extract claims from token here and do the needful
            
            if (param.Value == null)
            {
                context.Result = new BadRequestObjectResult("Object is null");
                return;
            }

            if (!context.ModelState.IsValid)
            {
                context.Result = new UnprocessableEntityObjectResult(context.ModelState);
            }

           //save token in header when making request
           //user logs in
           //user is issued a token
           //token is saved in session
           //whenever user makes a request, token is retrieved from session
           //sends request to api
           //the token gets added to the header
           //token is retrieved from header when it gets to the actionfilter
           //claims is retrieved from token
           //roles are retrieved from claims
           //user is allowed if he has the approved role for that opearation

        }

        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

       
    }
}
